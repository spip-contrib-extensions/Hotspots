<?php

if (!defined("_ECRIRE_INC_VERSION")) return;


function action_hotspot_sauver() {
	$id_article = $_GET["id_article"];
	
	if (!autoriser('modifier',"article",$id_article)) return;

	if ($_GET["forme"]) {
		$forme = $_GET["forme"];
		
		
		if ($forme == "no") $forme = "";
		else ecrire_meta("hotspot_forme", $forme);

		sql_updateq("spip_articles",
			array(
				"hotspot_forme" => $forme
			),
			"id_article=$id_article"
		);
		header("Location:index.php?exec=article&id_article=$id_article");  
		die();
		
	} else {
		sql_updateq("spip_articles",
			array(
				"hotspot_x1" => $_GET["x1"],
				"hotspot_y1" =>$_GET["y1"],
				"hotspot_x2" => $_GET["x2"],
				"hotspot_y2" =>$_GET["y2"]
			),
			"id_article=$id_article"
		);
	}
	die ("Modifier: $id_article");
}
