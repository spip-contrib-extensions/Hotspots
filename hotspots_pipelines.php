<?php

function hotspots_afficher_complement_objet($flux) {
	if ($flux['args']['type'] == "rubrique"
		AND $id=intval($flux['args']['id'])
		AND (autoriser('modifier',"rubrique",$id))) {

		$contexte = array('id_rubrique' => $id);
		$flux['data'] = recuperer_fond("ajouter_fond_hotspot", $contexte).$flux['data'];
	} else if ($flux['args']['type'] == "article"
		AND $id=intval($flux['args']['id'])
		AND (autoriser('modifier',"article",$id))) {
			$contexte = array('id_article' => $id);
			$flux['data'] = recuperer_fond("modifier_hotspot_article", $contexte).$flux['data'];
		}
	return $flux;

}

function hotspots_affiche_droite($flux) {

	if ($flux['args']['exec'] == "article"
		AND $id=intval($flux['args']['id_article'])
		AND (autoriser('modifier',"article",$id))) {

		$contexte = array('id_article' => $id);
		$flux['data'] = recuperer_fond("ajouter_hotspot_article", $contexte).$flux['data'];
	}
	return $flux;

}


function hotspots_declarer_tables_objets_sql ($tables){

	$tables['spip_articles']['field']['hotspot_forme'] = "varchar(10) DEFAULT ''";
	$tables['spip_articles']['field']['hotspot_x1'] = "bigint(21) NOT NULL DEFAULT '0'";
	$tables['spip_articles']['field']['hotspot_y1'] = "bigint(21) NOT NULL DEFAULT '0'";
	$tables['spip_articles']['field']['hotspot_x2'] = "bigint(21) NOT NULL DEFAULT '0'";
	$tables['spip_articles']['field']['hotspot_y2'] = "bigint(21) NOT NULL DEFAULT '0'";
	$tables['spip_articles']['field']['hotspot_direction'] = "varchar(3) NOT NULL DEFAULT ''";

	return $tables;
}

function hotspots_header_prive($flux) {
	$flux .= recuperer_fond("prive/hotspots_header_prive");
	return $flux;
}
