<?php


function formulaires_fond_hotspot_charger_dist($id){
	include_spip("inc/utils");
	$repertoire = sous_repertoire(_DIR_IMG, "hotspots");
	$img = $repertoire."hotspot$id.jpg";
	/*
	include_spip("inc/filtres_images_mini");
	$img =  image_reduire($img, 800,0);
	$img = extraire_attribut($img, "src");
	*/

	
	$valeurs = array(
		"id_rubrique"=>"$id",
		"upload_fond_hotspot" => ""
	);
	if (file_exists($img)) $valeurs["src_img"] = $img;

	$valeurs['_bigup_rechercher_fichiers'] = true;
	
	return $valeurs;
}


function formulaires_fond_hotspot_verifier_dist($id){
	$erreurs = array();
	
	return $erreurs;
}



function formulaires_fond_hotspot_traiter_dist($id){
	$repertoire = sous_repertoire(_DIR_IMG, "hotspots");
	$dest = $repertoire."hotspot$id.jpg";
	
	if (isset($_POST["utiliser_logo_rub"])  && strlen($_POST["utiliser_logo_rub"]) > 3) {
		$fichier = $_POST["utiliser_logo_rub"];
		copy ($fichier, $dest);	
	}
	
	if (isset($_POST["supprimer_fond_hotspot"])  && $_POST["supprimer_fond_hotspot"]){
		@unlink($dest);
	}

	if (isset($_FILES['upload_fond_hotspot']) && $_FILES['upload_fond_hotspot']['name']) {
		$fichier = $_FILES['upload_fond_hotspot']['name'];
		if (preg_match(",\.jpg$,", $fichier)) {
			$fichier = $_FILES['upload_fond_hotspot']['tmp_name'];
			rename($fichier, $dest);
		}
	}
	$retours = [
		'message_ok' => json_encode($_FILES),
		'editable' => true,
	];

	//return $retours;
}
