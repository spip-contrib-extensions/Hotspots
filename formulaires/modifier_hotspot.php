<?php

function formulaires_modifier_hotspot_charger_dist($id_article){

	$valeurs = array(
		"id_article"=>"$id_article"
	);

	
	return $valeurs;
}


function formulaires_modifier_hotspot_verifier_dist($id_article){
	$erreurs = array();
		
	return $erreurs;
}



function formulaires_modifier_hotspot_traiter_dist($id_article){	
	if ($_FILES["fichier_hotspot"]) {
		$repertoire = sous_repertoire(_DIR_IMG, "hotspot_point");
		$fichier = $_FILES['fichier_hotspot']['name'];
		$dest = "hotspot$id_article.png";
		if (preg_match(",\.png$,", $fichier)) {
			$fichier = $_FILES['fichier_hotspot']['tmp_name'];
			move_uploaded_file($fichier, $repertoire.$dest);
		}
	}
	if ($_POST["supprimer_fichier_hotspot"]){
		$repertoire = sous_repertoire(_DIR_IMG, "hotspot_point");
		$dest = "hotspot$id_article.png";
		@unlink($repertoire.$dest);
	}

	if ($_POST["hotspot_direction"]){
		$direction = $_POST["hotspot_direction"];
		if ($direction == "ef") $direction = "";
	
		sql_updateq("spip_articles",
			array(
				"hotspot_direction" =>$direction
			),
			"id_article=$id_article"
		);
	}

		header("Location:index.php?exec=article&id_article=$id_article");  
		die();

}
