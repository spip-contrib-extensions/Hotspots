<?php

function formulaires_ajouter_hotspot_charger_dist($id_article){
	$repertoire = sous_repertoire(_DIR_IMG, "hotspots");
	
	$res = sql_select("id_rubrique", "spip_articles", "id_article=$id_article");
	while ($row = sql_fetch($res)){
		$id_rubrique = $row["id_rubrique"];
	}
	
	
	$img = $repertoire."hotspot$id_rubrique.jpg";
//	$img =  image_reduire_net($img, 548,0);
//	$img = supprimer_timestamp(extraire_attribut($img, "src"));

	$valeurs = array(
		"id_article"=>"$id_article",
		"src_img" => $img
	);

	
	return $valeurs;
}


function formulaires_ajouter_hotspot_verifier_dist($id_article){
	$erreurs = array();
		
		
	if ($_POST["x"] > 0) {
		$x = $_POST["x"];
		$y = $_POST["y"];
		
		$l = $_POST["largeur_image"];
		$r = $_POST["image_mini_largeur_image"];
		
		
		if ($r > 0) {
			$x = round($x / $r * $l);
			$y = round($y / $r * $l);

			include_spip("inc/utils");
			$forme = lire_config("hotspot_forme");
			if (strlen($forme) < 1) $forme = "rectangle";


			sql_updateq("spip_articles",
				array(
					"hotspot_forme" => $forme,
					"hotspot_x1" => $x,
					"hotspot_y1" => $y,
					"hotspot_x2" => $x+100,
					"hotspot_y2" => $y+100
				),
				"id_article=$id_article"
			);
		}
		
		
	}
	return $erreurs;
}



function formulaires_ajouter_hotspot_traiter_dist($id_article){
}
