<?php

function hotspots_upgrade($nom_meta_base_version, $version_cible){

	$maj = array();
	$maj['create'] = array(
		array('maj_tables', array('spip_articles')),
	);
	 $maj['0.3.0'] = $maj['create'];

	$maj['0.4.1'] = array(
		array('sql_alter', "TABLE spip_articles ADD hotspot_direction varchar(3) NOT NULL DEFAULT '' AFTER hotspot_y2")
	);
	
	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}
