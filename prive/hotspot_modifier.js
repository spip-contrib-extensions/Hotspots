

$(document).on("fullscreenchange", function(){
	if (!$("#modifier_hotspot > div")) return;
	if (document.fullscreenElement) {
		re = $(window).width()/$(window).height();
		ri = $("#image_fond_hotspots").data("w") / $("#image_fond_hotspots").data("h");

		if (ri > re) $("#modifier_hotspot > div").css("width", (ri/re)*100+"%");
		console.log(re);
	} else {
		$("#modifier_hotspot > div").css("width", "auto");
	}


})


function switcher_hotspot_fullscreen() {
	$("#image_fond_hotspots").attr("src", $("#image_fond_hotspots").data("zoom"));
	$("#modifier_hotspot").toggleFullScreen();
	setTimeout(initier_hotspots, 1000);

}


function sauver_hotspot() {
	var id_article = $("#hotspot_article").val();
	var x1 = $("#hotspot_x1").val();
	var y1 = $("#hotspot_y1").val();
	var x2 = $("#hotspot_x2").val();
	var y2 = $("#hotspot_y2").val();
	positionner_fond_hotspot() ;
	
	$.ajax({
		method: "GET",
		url: "index.php",
		data: { action: "hotspot_sauver", id_article:id_article, x1:x1, y1:y1, x2:x2, y2:y2 }
	});
}

function sauver_forme_hotspot() {
		var forme = $("#hotspot_forme").val();
		var id_article = $("#hotspot_article").val();

		document.location = "index.php?action=hotspot_sauver&id_article="+id_article+"&forme="+forme;

}

function positionner_fond_hotspot() {
		$(".spip_hotspot").each(function() {
			var t = $(this);
			
			t.css("background-position", "-"+(parseInt(t.css("left"))+parseInt(t.css("margin-left"))+parseInt(t.css("border-left-width")))+"px -"
				+(parseInt(t.css("top"))+parseInt(t.css("margin-top"))+parseInt(t.css("border-top-width")))+"px" );
		});

}


function initier_hotspots() {
		$(".spip_hotspot").each(function() {
			var t = $(this);
			var img = $("#image_fond_hotspots");
			
			img.css("opacity", 0.4);
			var w = img.outerWidth();
			var h = img.outerHeight();
			console.log(w);
			
			var fichier = img.attr("src");
			
			t.css("background-image", "url("+fichier+")")
				.css("background-size", w+"px "+h+"px");
			positionner_fond_hotspot	();
			
		});

}

$(document).ready(function() {
	
	initier_hotspots();
	
	$("#hotspot_forme").on("change", function() {
		sauver_forme_hotspot();
	});
	
	$(".spip_hotspot_point.drag, .spip_hotspot_direction.drag").draggable({
		drag: function( event, ui ) {
			ui.position.left = Math.max(0, ui.position.left);
			ui.position.top = Math.max(0, ui.position.top);
			ui.position.top = Math.min(ui.position.top, $(this).parent().outerHeight());
			ui.position.left = Math.min(ui.position.left, $(this).parent().outerWidth());
			positionner_fond_hotspot() ;
		},

		stop: function( event, ui ) {
			var largeur_image= $("#largeur_image").val();
			var rapport = $(this).parent().outerWidth() / largeur_image;
			
			
			$("#hotspot_x1").val(Math.round(ui.position.left / rapport));
			$("#hotspot_y1").val(Math.round(ui.position.top / rapport) );
			$("#hotspot_x2").val(Math.round(ui.position.left / rapport)+100);
			$("#hotspot_y2").val(Math.round(ui.position.top / rapport)+100 );
			
			sauver_hotspot();
		}
	
	});
	$(".spip_hotspot_rectangle.drag, .spip_hotspot_oval.drag").draggable({
		drag: function( event, ui ) {
			ui.position.left = Math.max(0, ui.position.left);
			ui.position.top = Math.max(0, ui.position.top);

			var oH= $(this).outerHeight();
			var oW = $(this).outerWidth();
			
			ui.position.top = Math.min(ui.position.top, $(this).parent().outerHeight() - oH);
			ui.position.left = Math.min(ui.position.left, $(this).parent().outerWidth() - oW);
						
			$(".hotspot_drag_nw").css("left", ui.position.left).css("top", ui.position.top);
			$(".hotspot_drag_se").css("left", ui.position.left + oW).css("top", ui.position.top + oH);
			
			positionner_fond_hotspot();
		},

		stop: function( event, ui ) {
			var largeur_image= $("#largeur_image").val();
			var rapport = $(this).parent().outerWidth() / largeur_image;
			var oH= $(this).outerHeight();
			var oW = $(this).outerWidth();
			
			
			$("#hotspot_x1").val(Math.round(ui.position.left / rapport));
			$("#hotspot_y1").val(Math.round(ui.position.top / rapport) );
			$("#hotspot_x2").val(Math.round((ui.position.left+oW) / rapport));
			$("#hotspot_y2").val(Math.round((ui.position.top+oH) / rapport) );

			sauver_hotspot();
			positionner_fond_hotspot();
		}
	
	});

	
	
	$(".hotspot_drag_nw").draggable({
		drag: function( event, ui ) {
			ui.position.left = Math.max(0, ui.position.left);
			ui.position.top = Math.max(0, ui.position.top);
			
			var left_se = parseInt($(".hotspot_drag_se").css("left"));
			var top_se = parseInt($(".hotspot_drag_se").css("top"));
			
			ui.position.left = Math.min(ui.position.left, left_se);
			ui.position.top = Math.min(ui.position.top, top_se );
			
			$(".spip_hotspot.drag").css("top", ui.position.top+"px")
				.css("left", ui.position.left+"px")
				.css("width", (left_se - ui.position.left)+"px")
				.css("height", (top_se - ui.position.top)+"px");

			positionner_fond_hotspot();
		},
		stop: function( event, ui ) {
			var largeur_image= $("#largeur_image").val();
			var rapport = $(this).parent().outerWidth() / largeur_image;
			
			
			$("#hotspot_x1").val(Math.round(ui.position.left / rapport));
			$("#hotspot_y1").val(Math.round(ui.position.top / rapport) );


			sauver_hotspot();
		}

	});
	$(".hotspot_drag_se").draggable({
		drag: function( event, ui ) {

			var left_nw = parseInt($(".hotspot_drag_nw").css("left"));
			var top_nw = parseInt($(".hotspot_drag_nw").css("top"));

			ui.position.left = Math.max(left_nw, ui.position.left);
			ui.position.top = Math.max(top_nw, ui.position.top);

			ui.position.top = Math.min(ui.position.top, $(this).parent().outerHeight());
			ui.position.left = Math.min(ui.position.left, $(this).parent().outerWidth());

			$(".spip_hotspot.drag")
				.css("width", (ui.position.left - left_nw)+"px")
				.css("height", (ui.position.top - top_nw)+"px");

			positionner_fond_hotspot();

		},
		stop: function( event, ui ) {
			var largeur_image= $("#largeur_image").val();
			var rapport = $(this).parent().outerWidth() / largeur_image;

			$("#hotspot_x2").val(Math.round(ui.position.left / rapport));
			$("#hotspot_y2").val(Math.round(ui.position.top / rapport) );

			sauver_hotspot();
		}
		

	});

	$('.formulaire_fond_hotspot input.bigup').bigup({}, {
			complete: function(){
					$('.formulaire_fond_hotspot form').submit();
			}
	});

	
});	